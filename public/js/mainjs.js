$(document).ready(function () {
    // date picker for From date in list new items
    if ($('#itemFromDate').type != 'date') {
        $('#itemFromDate').datepicker({dateFormat: 'dd/mm/yy'}).val();
    }
    // date picker for To date
    if ($('#itemToDate').type != 'date') {
        $('#itemToDate').datepicker({dateFormat: 'dd/mm/yy'}).val();
    }

    // date picker for From date in search page
    if ($('#searchItemFromDate').type != 'date') {
        $('#searchItemFromDate').datepicker({dateFormat: 'dd/mm/yy'}).val();
    }
    // date picker for To date
    if ($('#searchItemToDate').type != 'date') {
        $('#searchItemToDate').datepicker({dateFormat: 'dd/mm/yy'}).val();
    }


    // date picker for From date in account items popup
    if ($('#accItemFromDate').type != 'date') {
        $('#accItemFromDate').datepicker({dateFormat: 'dd/mm/yy'}).val();
        $('#accItemFromDate').css('zIndex', '9999');
        $('#accItemFromDate').css({ 'position': 'relative' });
    }
    // date picker for To date
    if ($('#accItemToDate').type != 'date') {
        $('#accItemToDate').datepicker({dateFormat: 'dd/mm/yy'}).val();
        $('#accItemToDate').css('zIndex', '9999');
        $('#accItemToDate').css({ 'position': 'relative' });
    }

    if ($("#sessionName").val() == null
        || $("#sessionName").val() == "" ){
        $("#user-dropdown").prop("disabled",true);
        $("#user-dropdown").css("visibility","hidden");
    }

    $("#main-navbar > .navbar-nav > li").click(function() {
        // remove classes from all
        $("li").removeClass("active");
        // add class to the one we clicked
        $(this).addClass("active");
    });

    function getLocationUsingIP(){
        // get the user latitude longitude based upon IP
        $.ajax({
            url:"http://ipinfo.io/json",
            async:false,
            success:function(result){
                var location = result.loc;
                location = location.split(",");
                $("#userLat").val(location[0]);
                $("#userLng").val(location[1]);
            }
        });
    }

    $("#simpleSearchBtn").click(getLocationUsingIP());
    $("#advancedSearchBtn").click(getLocationUsingIP());

    // set the item latitude & longitude in the form so that it can be sent to server
    $("#itemSubmit").click(function(){
        var address = $("#itemAddress").val();
        $.ajax({
            //get latitude longitude via google api based upon address
            url: "http://maps.googleapis.com/maps/api/geocode/json?address="+address,
            async: false,
            success: function(result){
                if(result.status=="OK"){
                    $("#itemLat").val(result.results[0].geometry.location.lat);
                    $("#itemLng").val(result.results[0].geometry.location.lng);
                }
            }
        });
    });

    // add or remove required fields in search page
    $("#advancedSearchBtn").click(function(){
        if($("#isAdvSearch").val()=="true"){
            $("#advancedSearchItemName").attr('required',false);
            $("#searchItemCategory").attr('required',false);
            $("#searchItemFromDate").attr('required',false);
            $("#searchItemToDate").attr('required',false);
            $("#maxItemFee").attr('required',false);
            $("#maxDistance").attr('required',false);
            $("#simpleSearchBtn").show();
            $("#searchItemName").css("visibility","visible");
            $("#isAdvSearch").val("false");
        }else{
            $("#searchItemName").attr('required',false);
            $("#isAdvSearch").val("true");
            $("#searchItemName").css("visibility","hidden");
            $("#simpleSearchBtn").hide();
        }
    });
    //by default everytime search page is available
    $("#isAdvSearch").val("false");
});

function has_id(id){try{var tmp=document.getElementById(id).value;}catch(e){return false;}
    return true;}
function has_name(nm){try{var tmp=cfrm.nm.type;}catch(e){return false;}
    return true;}
function $$(id){if(!has_id(id)&&!has_name(id)){alert("Field "+id+" does not exist!\n Form validation configuration error.");return false;}
    if(has_id(id)){return document.getElementById(id).value;}else{return;}}
function $val(id){return document.getElementById(id);}
function trim(id){$val(id).value=$val(id).value.replace(/^\s+/,'').replace(/\s+$/,'');}
var required={field:[],add:function(name,type,mess){this.field[this.field.length]=[name,type,mess];},out:function(){return this.field;},clear:function(){this.field=[];}};var validate={check:function(cform){var error_message='Please fix the following errors:\n\n';var mess_part='';var to_focus='';var tmp=true;for(var i=0;i<required.field.length;i++){if(this.checkit(required.field[i][0],required.field[i][1],cform)){}else{error_message=error_message+required.field[i][2]+' must be supplied\n';if(has_id(required.field[i][0])&&to_focus.length===0){to_focus=required.field[i][0];}
    tmp=false;}}
    if(!tmp){alert(error_message);}
    if(to_focus.length>0){document.getElementById(to_focus).focus();}
    return tmp;},checkit:function(cvalue,ctype,cform){if(ctype=="NOT_EMPTY"){if(this.trim($$(cvalue)).length<1){return false;}else{return true;}}else if(ctype=="EMAIL"){exp=/^[a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;if($$(cvalue).match(exp)==null){return false;}else{return true;}}},trim:function(s){if(s.length>0){return s.replace(/^\s+/,'').replace(/\s+$/,'');}else{return s;}}};