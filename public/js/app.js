(function(){
    //load the module
    var app = angular.module('rentwhatihave',['ui.router','js-data']);

    //controller
   app.controller('rentwhatihaveCtrl',function($scope,$http){
       $scope.itemInsuranceAngular=true;
       $scope.itemImage = "";
       // get the categories from database
       $scope.categories = "";
       $http.get("/categories").success(function(result) {
               $scope.categories = result;
       });

       // on change of category, set the value as id instead of number:id
       $scope.itemCategoryChanged = function($element){
           $element.attributes['id'].value = $scope.itemCategoryAngular;
       };
   })
       .directive('itemImage', [function () {
           return {
               restrict: 'A',
               link: function (scope, elem, attrs) {
                   var reader = new FileReader();
                   reader.onload = function (e) {
                       scope.itemImage = e.target.result;
                       scope.$apply();
                   };

                   elem.on('change', function() {
                       reader.readAsDataURL(elem[0].files[0]);
                   });
               }
           };
       }]);
})()