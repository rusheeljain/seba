package validations;

import helpers.CommonMethods;
import play.data.DynamicForm;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Rusheel on 04/06/2015.
 */
public class ListItemValidations {

    private static String errorMessage=null;

    public static String validation(DynamicForm form){
        setErrorMessage(null);

        setErrorMessage(requiredFieldsCheck(form));
        if(getErrorMessage()==null){
            setErrorMessage(typeErrorCheck(form));
            if (getErrorMessage()==null){
                setErrorMessage(valueAndLengthCheck(form));
            }
        }
        return getErrorMessage();
    }

    private static String requiredFieldsCheck(DynamicForm form){

        if(form.get("itemName")==null){
            setErrorMessage("Item Name required!");
            return getErrorMessage();
        }
        if(form.get("itemCategory")==null){
            setErrorMessage("Item Category required!");
            return getErrorMessage();
        }
        if(form.get("itemTitle")==null){
            setErrorMessage("Item Title required!");
            return getErrorMessage();
        }
        if(form.get("itemAddress")==null){
            setErrorMessage("Address required!");
            return getErrorMessage();
        }
        if(form.get("itemFromDate")==null){
            setErrorMessage("From Date required!");
            return getErrorMessage();
        }
        if(form.get("itemToDate")==null){
            setErrorMessage("To Date required!");
            return getErrorMessage();
        }
        if(form.get("itemFee")==null){
            setErrorMessage("Item Fee required!");
            return getErrorMessage();
        }
        if(form.get("itemDeposit")==null) {
            setErrorMessage("Deposit required!");
            return getErrorMessage();
        }
        return getErrorMessage();
    }

    private static String typeErrorCheck(DynamicForm form){
        //check for Item Name
        if(regexMatch("[^a-z0-9 ]",form.get("itemName"))) {
            setErrorMessage("Item name can be only alphanumeric along with spaces!");
            return getErrorMessage();
        }
        // check for category id
        try {
            Integer.parseInt(form.get("itemCategory"));
        }catch (NumberFormatException e){
            setErrorMessage("Invalid Item Category!");
            return getErrorMessage();
        }
        // check for Item Title
        if(regexMatch("[^a-z0-9 ]",form.get("itemTitle"))) {
            setErrorMessage("Item Title can be only alphanumeric along with spaces!");
            return getErrorMessage();
        }
        // check for Item Description
        if(regexMatch("[^a-z0-9 .!*,]",form.get("itemDescription"))) {
            setErrorMessage("Item Description can be only alphanumeric along with spaces, dot, comma, stars and exclamation!");
            return getErrorMessage();
        }
        // check for from date
        if(isInvalidDate(form.get("itemFromDate"))){
            setErrorMessage("Invalid From Date!");
            return getErrorMessage();
        }
        // check for to date
        if(isInvalidDate(form.get("itemToDate"))){
            setErrorMessage("Invalid To Date!");
            return getErrorMessage();
        }
        // check for item fee
        try {
            Float.parseFloat(form.get("itemFee"));
        }catch (NumberFormatException e){
            setErrorMessage("Item Fee should be in currency format!");
            return getErrorMessage();
        }
        // check for item deposit
        try {
            Float.parseFloat(form.get("itemDeposit"));
        }catch (NumberFormatException e){
            setErrorMessage("Item Deposit should be in currency format!");
            return getErrorMessage();
        }
        // check for item insurance
        if (form.get("itemInsurance")!=null &&
                !form.get("itemInsurance").equalsIgnoreCase("on")){
            setErrorMessage("Please check if value for Insurance checkbox is correct!");
            return errorMessage;
        }
        return getErrorMessage();
    }

    private static String valueAndLengthCheck(DynamicForm form){
        // item name cant be empty string
        String itemName = form.get("itemName").trim();
        if (itemName.length()==0){
            setErrorMessage("Item Name can not have only spaces!");
            getErrorMessage();
        }
        //category Id cant be zero or negative
        int categoryId = Integer.parseInt(form.get("itemCategory"));
        if(categoryId<=0){
            setErrorMessage("Invalid Item Category!");
            return getErrorMessage();
        }
        // item title cant be empty string
        String itemTitle = form.get("itemTitle").trim();
        if (itemTitle.length()==0){
            setErrorMessage("Item Title can not be empty!");
            getErrorMessage();
        }
        // from date should be earlier than to date
        Date fromDate = CommonMethods.convertToDate(form.get("itemFromDate"));
        Date toDate = CommonMethods.convertToDate(form.get("itemToDate"));
        if (toDate.before(fromDate)){
            setErrorMessage("From Date cannot be before To Date!");
            return getErrorMessage();
        }
        // item fee must not be negative and should have only two digits after decimal
        String itemFeeString = form.get("itemFee");
        Float itemFee = Float.parseFloat(itemFeeString);
        if(itemFeeString.indexOf(".")!=-1) {
            if (itemFeeString.length() - (itemFeeString.indexOf(".") + 1) > 2) { // as listNew starts from zero
                setErrorMessage("Item Fee should have only two digits after decimal!");
                return getErrorMessage();
            }
        }
        if (itemFee<0){
            setErrorMessage("Item Fee cannot be negative!");
            return getErrorMessage();
        }
        // item deposit must not be negative and should have only two digits after decimal
        String itemDepositString = form.get("itemDeposit");
        Float itemDeposit = Float.parseFloat(itemDepositString);
        if(itemDepositString.indexOf(".")!=-1) {
            if (itemDepositString.length() - (itemDepositString.indexOf(".") + 1) > 2) {//since listNew starts from zero
                setErrorMessage("Item Deposit should have only two digits after decimal!");
                return getErrorMessage();
            }
        }
        if (itemDeposit<0){
            setErrorMessage("Item Deposit cannot be negative!");
            return getErrorMessage();
        }
        return getErrorMessage();
    }
    public static String getErrorMessage() {
        return errorMessage;
    }

    public static void setErrorMessage(String errorMessage) {
        ListItemValidations.errorMessage = errorMessage;
    }

    private static boolean regexMatch(String regex, String matcher){
        Pattern p = Pattern.compile(regex,Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(matcher);
        return m.find();

    }

    private static boolean isInvalidDate(String inputDate){
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        try {
            format.parse(inputDate);
        } catch (ParseException e) {
           return true;
        }
        return false;
    }
}
