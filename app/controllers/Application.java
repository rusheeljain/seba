package controllers;

import models.Categories;
import models.ItemDetails;
import helpers.ShowItems;
import models.UserDetails;
import play.data.DynamicForm;
import play.data.Form;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import validations.ListItemValidations;
import views.html.*;

import java.sql.Blob;
import java.util.List;

public class Application extends Controller {


    //Private attributes
    private static String itemName;
    private static int categoryId;
    private static String itemTitle;
    private static String itemAddress;
    private static String itemDescription;
    private static String itemFromDate;
    private static String itemToDate;
    private static float itemDeposit;
    private static float itemFee;
    private static Blob itemImage;
    private static String itemInsurance;
    private static String itemLatitude;
    private static String itemLongitude;

    // success or error message
    private static String message;

    @Transactional(readOnly = true)
    public static Result home() {
        if (LoginController.isValidSession()) {
            return temporaryRedirect("/search");
        }
        return ok(home.render(null, false));
    }


    @Transactional(readOnly = true)
    public static Result listedItems() {
        if (!LoginController.isValidSession()) {
            return temporaryRedirect("/");
        }
        List<ShowItems> showAllItems = new ShowItems().getUserListedItems();
        return ok(views.html.accountItems.render(showAllItems,"listed"));
    }

    @Transactional(readOnly = true)
    public Result rentedItems(){
        if (!LoginController.isValidSession()) {
            return temporaryRedirect("/");
        }
        List<ShowItems> showAllItems = new ShowItems().getUserRentedItems();
        return ok(views.html.accountItems.render(showAllItems,"rented"));
    }

    @Transactional(readOnly = true)
    public static Result listNewItem() {
        if (!LoginController.isValidSession()) {
            return temporaryRedirect("/");
        }
        return ok(listNew.render(Categories.findAll(), message, false));
    }

    @Transactional(readOnly = true)
    public static Result howitworks() {
        return ok(howitworks.render(null));
    }

    @Transactional(readOnly = true)
    public static Result contactus() {
        return ok(contactus.render());
    }

    //SEARCH
    @Transactional(readOnly = true)
    public static Result search() {
        if (!LoginController.isValidSession()) {
            return temporaryRedirect("/");
        }
        return ok(search.render(Categories.findAll()));
    }


    @Transactional(readOnly = true)
    public Result getSearchResults() {
        if (!LoginController.isValidSession()) {
            return temporaryRedirect("/");
        }
        DynamicForm form = Form.form().bindFromRequest();
        // user id
        int myId = UserDetails.findByEmail(session().get("email")).get(0).id;
        return ok(searchResults.render(new ShowItems().getItems(form, myId)));
    }

    @Transactional(readOnly = true)
    public Result redirectToSearchPage() {
        if (!LoginController.isValidSession()) {
            return temporaryRedirect("/");
        }

        return temporaryRedirect("/search");
    }


    @Transactional(readOnly = true)
    public static Result getCategories() {
        return ok(Json.toJson(Categories.findAll()));
    }

    @Transactional
    public static Result addNewItem() {

        if (!LoginController.isValidSession())
            return temporaryRedirect("/");
        DynamicForm form = Form.form().bindFromRequest();
        String message = null;
        //validations check
        if ((message = ListItemValidations.validation(form)) != null) {
            return ok(listNew.render(Categories.findAll(), message, true));
        } else {
            // initialize private variables
            initializeValues(form);

            // fill ItemDetails model
            ItemDetails itemDetails = new ItemDetails();
            itemDetails.itemName = itemName;
            itemDetails.categoryId = categoryId;
            itemDetails.itemTitle = itemTitle;
            itemDetails.itemAddress = itemAddress;
            itemDetails.itemDescription = itemDescription;

            itemDetails.itemFromDate = itemFromDate;
            itemDetails.itemToDate = itemToDate;
            itemDetails.itemFee = itemFee;
            itemDetails.itemDeposit = itemDeposit;
            itemDetails.itemInsurance = itemInsurance;

            // get the owner id
            itemDetails.ownerId = UserDetails.findByEmail(session("email")).get(0).id;
            //get latitudes & longitudes
            itemDetails.itemLatitude = itemLatitude;
            itemDetails.itemLongitude = itemLongitude;

            //when a new item is listed, rent request status must be 'no' i.e. not yet rented
            itemDetails.acceptRentRequest = "no";
            // persist into DB
            itemDetails.save();
            // display success message
            message = "Item has been successfully listed!";

            return ok(listNew.render(Categories.findAll(), message, false));
        }
    }

    //Save content from form into class variables.
    private static void initializeValues(DynamicForm form) {
        Application.itemName = form.get("itemName");
        Application.categoryId = Integer.parseInt(form.get("itemCategory"));
        Application.itemTitle = form.get("itemTitle");
        Application.itemAddress = form.get("itemAddress");
        Application.itemDescription = form.get("itemDescription");
        Application.itemFromDate = form.get("itemFromDate");
        Application.itemToDate = form.get("itemToDate");
        Application.itemFee = Float.parseFloat(form.get("itemFee"));
        Application.itemDeposit = Float.parseFloat(form.get("itemDeposit"));
        Application.itemInsurance = form.get("itemInsurance");
        Application.itemLatitude = form.get("itemLat");
        Application.itemLongitude = form.get("itemLng");
    }

    @Transactional
    public Result modifyItemDetails() {
        if (!LoginController.isValidSession()) {
            return temporaryRedirect("/");
        }
        DynamicForm form = Form.form().bindFromRequest();
        ShowItems showItems = new ShowItems();
        if (form.get("action").equalsIgnoreCase("update")) {
            showItems.updateListedItems(form);
        } else {
            showItems.deleteListedItem(form);
        }
        return temporaryRedirect("/listedItems");
    }

    // when new request for accept/reject rent comes
    @Transactional
    public Result processRentRequest(){
        if (!LoginController.isValidSession()) {
            return temporaryRedirect("/");
        }
        DynamicForm form = Form.form().bindFromRequest();
        ShowItems showItems = new ShowItems();
        int itemId = Integer.parseInt(form.get("requestItemId"));
        if (form.get("action").equalsIgnoreCase("accept")) {
            showItems.acceptRentRequest(itemId);
        } else {
            showItems.rejectRentRequest(itemId);
        }
        return temporaryRedirect("/rentRequest");
    }

    @Transactional
    public Result showRentRequests(){
        if (!LoginController.isValidSession()) {
            return temporaryRedirect("/");
        }
        return ok(rentRequests.render(getRentRequests()));

    }

    private List<ItemDetails> getRentRequests(){
        return
                new ItemDetails().getRentRequestsForUser(
                        UserDetails.findByEmail(session("email")).get(0).id);
    }


    private int getNumberOfRequests(){

               return  new ItemDetails().getRentRequestsForUser(
                        UserDetails.findByEmail(session("email")).get(0).id).size();

    }

    //when user wants to rent an item after search
    @Transactional
    public Result applyForRent(){
        if (!LoginController.isValidSession()) {
            return temporaryRedirect("/");
        }
        DynamicForm form = Form.form().bindFromRequest();
        new ItemDetails().saveRentRequest(
                Integer.parseInt(form.get("itemId")),
                UserDetails.findByEmail(session("email")).get(0).id);
        return temporaryRedirect("/rentedItems");
    }

}



