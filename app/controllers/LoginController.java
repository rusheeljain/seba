package controllers;

import models.UserDetails;
import org.mindrot.jbcrypt.BCrypt;
import play.data.DynamicForm;
import play.data.Form;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.home;

import java.util.List;

/**
 * Created by Rusheel on 16/06/2015.
 */
public class LoginController extends Controller {
    private static String email;
    private static String password;
    private static String name;


    @Transactional(readOnly = true)
    public static Result authenticate() {
        try {
            DynamicForm form = Form.form().bindFromRequest();
            email = form.get("userEmail");
            password = form.get("userPassword");
            if (!(email == null || email.isEmpty()
                    || password == null || password.isEmpty())) {
                // check for user details in db
                List<UserDetails> userDetail = UserDetails.findByEmail(email);
                if (userDetail.size() == 1) {
                    UserDetails user = userDetail.get(0);
                    if (user.email.equalsIgnoreCase(email)
                            && BCrypt.checkpw(password, user.password)) {
                        // valid user
                        session("email", user.email);
                        session("name", user.name);
                        session("isLoggedIn", "true");
                        return temporaryRedirect("/search");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ok(home.render("Invalid Credentials", true));
    }

    @Transactional
    public static Result signup() {
        DynamicForm form = Form.form().bindFromRequest();
        name = form.get("signupUserName");
        email = form.get("signupUserEmail");
        password = form.get("signupUserPassword");
        // check if user with same email exists
        List<UserDetails> userDetail = UserDetails.findByEmail(email);
        if (userDetail.size() == 1) {
            // invalid user
            return ok(home.render("This email is already in use", true));
        }
        UserDetails user = new UserDetails();
        user.name = name;
        user.email = email;
        user.password = user.createPasswordHash(password);
        user.save();
        session("email", email);
        session("name", name);
        session("isLoggedIn", "true");
        return temporaryRedirect("/search");
    }

    @Transactional(readOnly = true)
    public static boolean isValidSession() {
        String email = session("email");
        // check if user with same email exists
        List<UserDetails> userDetail = UserDetails.findByEmail(email);
        if (userDetail.size() == 1) {
            return true;
        }
        return false;
    }

    public static Result logout() {
        //discard session

        session().clear();
        session("isLoggedIn", "false");
        return ok(home.render("Logout successful", false));
    }

}
