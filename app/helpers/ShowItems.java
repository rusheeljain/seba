package helpers;

import models.Categories;
import models.ItemDetails;
import models.UserDetails;
import play.data.DynamicForm;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static play.mvc.Controller.session;

/**
 * Created by Rusheel on 27/06/2015.
 */
public class ShowItems {

    public int itemId;
    public String itemName;
    public String categoryName;
    public String itemTitle;
    public String itemDescription;
    public String itemToDate;
    public String itemFromDate;
    public float itemFee;
    public float itemDeposit;
    public Double itemDistance;
    public int renterId;
    public String status;

    public List<ShowItems> getItems(DynamicForm form,int myId) {

        ItemDetails itemDetails;
        List<ShowItems> showItemsList;
        ShowItems showItems;
        CommonMethods commonMethods = new CommonMethods();
        boolean isAdvancedSearch;
        Double distance;

        // latitude, longitude of the item
        Double lat1;
        Double lng1;

        // latitue longitude of the user
        Double lat2 = Double.parseDouble(form.get("userLat"));
        Double lng2 = Double.parseDouble(form.get("userLng"));

        // first get all the possible items
        List<ItemDetails> itemDetailsList = ItemDetails.findAll();
        Iterator<ItemDetails> it = itemDetailsList.iterator();
        showItemsList = new ArrayList<ShowItems>();

        // get the maximum distance
        Double allowedDistance;

        // is advanced search
        isAdvancedSearch = Boolean.parseBoolean(form.get("isAdvSearch"));
        if (isAdvancedSearch) {
            allowedDistance = Double.parseDouble(form.get("maxDistance"));
        } else {
            allowedDistance = 50.0; // in kilometers
        }
        //check for search conditions
        while (it.hasNext()) {
            itemDetails = it.next();
            lat1 = Double.parseDouble(itemDetails.itemLatitude);
            lng1 = Double.parseDouble(itemDetails.itemLongitude);
            if ((distance = commonMethods.distance(lat1, lat2, lng1, lng2, 0, 0)) <= allowedDistance) {
                if (isItemRequired(form, itemDetails, isAdvancedSearch,myId)) {
                    showItems = new ShowItems();
                    showItems.itemDistance = distance;
                    showItems.itemId = itemDetails.id;
                    showItems.itemName = itemDetails.itemName;
                    showItems.categoryName = Categories.findByCategoryId(itemDetails.categoryId).get(0).name;
                    showItems.itemTitle = itemDetails.itemTitle;
                    showItems.itemDescription = itemDetails.itemDescription;
                    showItems.itemToDate = itemDetails.itemToDate;
                    showItems.itemFromDate = itemDetails.itemFromDate;
                    showItems.itemFee = itemDetails.itemFee;
                    showItems.itemDeposit = itemDetails.itemDeposit;
                    showItemsList.add(showItems);
                }
            }
        }
        return showItemsList;
    }

    private static boolean isItemRequired(DynamicForm form, ItemDetails itemDetails, boolean isAdvancedSearch, int myId) {

        // if I own this item, do not show in search results
        if(itemDetails.ownerId == myId){
            return false;
        }

        // if Item is already rented, do not show in search results
        if(itemDetails.renterId!=0){
            return false;
        }

        //basic search
        if (!isAdvancedSearch) {
            if (form.get("searchItemName") != null && form.get("searchItemName") != "") {
                return itemDetails.itemName.toLowerCase().trim().contains(form.get("searchItemName").toLowerCase().trim());
            } else {
                return false;
            }
        }

        // advanced search
        // search for item with item name
        if (!itemDetails.itemName.toLowerCase().trim().contains(form.get("searchItemName").toLowerCase().trim())) {
            return false;
        }
        // search in item category
        if (!(itemDetails.categoryId == Integer.parseInt(form.get("searchItemCategory")))) {
            return false;
        }

        // search from date
        if (CommonMethods.convertToDate(form.get("searchItemFromDate")).
                before(CommonMethods.convertToDate(itemDetails.itemFromDate)))
            return false;

        // search to date
        if (CommonMethods.convertToDate((form.get("searchItemToDate"))).
                after(CommonMethods.convertToDate(itemDetails.itemToDate)))
            return false;

        // rental fee
        if (Integer.parseInt(form.get("maxItemFee")) < itemDetails.itemFee)
            return false;

        return true;

    }

    public List<ShowItems> getUserRentedItems() {
        // first get all the required items
        List<ItemDetails> itemDetailsList =
                ItemDetails.getItemDetailsByRenterID
                        (UserDetails.findByEmail(session("email")).get(0).id); // renter id should be equal to the id of logged-in user
        return putItemsInList(itemDetailsList);
    }

    public List<ShowItems> getUserListedItems() {
        // first get all the required items
        List<ItemDetails> itemDetailsList =
                ItemDetails.getItemDetailsByOwnerID(UserDetails.findByEmail(session("email")).get(0).id);
        return putItemsInList(itemDetailsList);
    }

    private List<ShowItems> putItemsInList(List<ItemDetails> itemDetailsList){
        ItemDetails itemDetails;
        List<ShowItems> showItemsList;
        ShowItems showItems;
        Iterator<ItemDetails> it = itemDetailsList.iterator();
        showItemsList = new ArrayList<ShowItems>();
        while (it.hasNext()) {
            itemDetails = it.next();
            showItems = new ShowItems();
            showItems.itemId = itemDetails.id;
            showItems.itemName = itemDetails.itemName;
            showItems.categoryName = Categories.findByCategoryId(itemDetails.categoryId).get(0).name;
            showItems.itemTitle = itemDetails.itemTitle;
            showItems.itemDescription = itemDetails.itemDescription;
            showItems.itemToDate = itemDetails.itemToDate;
            showItems.itemFromDate = itemDetails.itemFromDate;
            showItems.itemFee = itemDetails.itemFee;
            showItems.itemDeposit = itemDetails.itemDeposit;
            showItems.renterId = itemDetails.renterId;
            showItems.status = itemDetails.acceptRentRequest;
            showItemsList.add(showItems);
        }
        return showItemsList;
    }

    public void updateListedItems(DynamicForm form) {
        ItemDetails itemDetails = new ItemDetails();
        itemDetails.itemName = form.get("accItemName");
        itemDetails.itemTitle = form.get("accItemTitle");
        itemDetails.itemDescription = form.get("accItemDescription");
        itemDetails.itemFromDate = form.get("accItemFromDate");
        itemDetails.itemToDate = form.get("accItemToDate");
        itemDetails.itemFee = Float.parseFloat(form.get("accItemFee"));
        itemDetails.itemDeposit = Float.parseFloat(form.get("accItemDeposit"));
        itemDetails.id = Integer.parseInt(form.get("itemId"));
        itemDetails.update(itemDetails);
    }

    public void deleteListedItem(DynamicForm form){
        ItemDetails itemDetails = new ItemDetails();
        int itemId = Integer.parseInt(form.get("itemId"));
        itemDetails.delete(itemId);
    }

    public void acceptRentRequest(int itemId){
        ItemDetails itemDetails = new ItemDetails();
        itemDetails.acceptRentRequest(itemId);
    }

    public void rejectRentRequest(int itemId){
        new ItemDetails().deleteRentRequest(itemId);
    }



}
