package models;

import org.mindrot.jbcrypt.BCrypt;
import play.data.validation.Constraints;
import play.db.jpa.JPA;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by Rusheel on 16/06/2015.
 */
@Entity
public class UserDetails {
    @Id
    @GeneratedValue
    public int id;

    @Constraints.Required
    public String name;

    @Constraints.Email
    public String email;

    @Constraints.Required
    public String password;


    public static List<UserDetails> findAll(){
        TypedQuery<UserDetails> query;
        query = JPA.em().createQuery("SELECT u FROM UserDetails u", UserDetails.class);
        return query.getResultList();
    }

    public static List<UserDetails> findByEmail(String email){
        TypedQuery<UserDetails> query;
        query = JPA.em().createQuery("SELECT u FROM UserDetails u where u.email=:email", UserDetails.class);
        query.setParameter("email",email);
        return query.getResultList();
    }

    public String createPasswordHash(String password){
        this.password = BCrypt.hashpw(password, BCrypt.gensalt());
        return this.password;
    }

    public void save(){
        JPA.em().persist(this);
    }
}
