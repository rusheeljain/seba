package models;

import play.data.validation.Constraints;
import play.db.jpa.JPA;

import javax.persistence.*;
import javax.validation.Constraint;
import java.util.Collection;
import java.util.List;

@Entity
public class Categories {

    @Id
    @GeneratedValue
    public int id;

    @Constraints.Required
    public String name;

    public static List<Categories> getCategoryIdbyName(String name){
        TypedQuery<Categories> query;
        query = JPA.em().createQuery("select c from Categories c where c.name=:name",Categories.class);
        query.setParameter("name",name);
        return query.getResultList();
    }

    public static List<Categories> findAll(){
        TypedQuery<Categories> query;
        query = JPA.em().createQuery("SELECT c FROM Categories c", Categories.class);
        return query.getResultList();
    }

    public static List<Categories> findByCategoryId(int id){
        TypedQuery<Categories> query;
        query = JPA.em().createQuery("SELECT c FROM Categories c where c.id=:id", Categories.class);
        query.setParameter("id",id);
        return query.getResultList();
    }
}
