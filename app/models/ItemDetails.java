package models;

import play.data.validation.Constraints;
import play.db.jpa.JPA;

import javax.persistence.*;
import java.sql.Blob;
import java.sql.Date;
import java.util.List;

@Entity
public class ItemDetails {
    @Id
    @GeneratedValue
    public int id;


    @Constraints.Required
    @Constraints.MaxLength(50)
    public String itemName;

    @Constraints.Required
    public int categoryId;

    @Constraints.Required
    @Constraints.MaxLength(50)
    public String itemTitle;


    @Constraints.Required
    public String itemAddress;

    @Constraints.MaxLength(200)
    public String itemDescription;

    @Constraints.Required
    public String itemFromDate;

    @Constraints.Required
    public String itemToDate;

    @Constraints.Required
    public float itemFee;

    @Constraints.Required
    public float itemDeposit;

    public String itemInsurance;

    public int ownerId;
    public int renterId;
    public String itemLatitude;
    public String itemLongitude;
    public String acceptRentRequest;

    //public Blob itemImage;


    public static List<ItemDetails> findAll() {
        TypedQuery<ItemDetails> query;
        query = JPA.em().createQuery("SELECT i FROM ItemDetails i", ItemDetails.class);

        return query.getResultList();
    }


    public void save() {
        JPA.em().persist(this);
    }


    public static List<ItemDetails> getItemDetailsByOwnerID(int ownerId) {
        TypedQuery<ItemDetails> query;

        query = JPA.em().createQuery("SELECT i FROM ItemDetails i where i.ownerId=:ownerId", ItemDetails.class);
        query.setParameter("ownerId", ownerId);
        return query.getResultList();
    }

    public static List<ItemDetails> getItemDetailsByRenterID(int renterId) {
        TypedQuery<ItemDetails> query;

        query = JPA.em().createQuery("SELECT i FROM ItemDetails i " +
                "where i.renterId=:renterId", ItemDetails.class);
        query.setParameter("renterId", renterId);
        return query.getResultList();
    }

    public void update(ItemDetails itemDetails) {
        Query query;
        query = JPA.em().createQuery(
                "UPDATE ItemDetails set itemName=:itemName," +
                        "itemTitle=:itemTitle," +
                        "itemDescription=:itemDescription," +
                        "itemFromDate=:itemFromDate," +
                        "itemToDate=:itemToDate," +
                        "itemFee=:itemFee," +
                        "itemDeposit=:itemDeposit where id=:id");
        query.setParameter("itemName", itemDetails.itemName);
        query.setParameter("itemTitle", itemDetails.itemTitle);
        query.setParameter("itemDescription", itemDetails.itemDescription);
        query.setParameter("itemFromDate", itemDetails.itemFromDate);
        query.setParameter("itemToDate", itemDetails.itemToDate);
        query.setParameter("itemFee", itemDetails.itemFee);
        query.setParameter("itemDeposit", itemDetails.itemDeposit);

        query.setParameter("id", itemDetails.id);
        query.executeUpdate();

    }

    public void delete(int id) {
        Query query;
        query = JPA.em().createQuery(
                "delete from ItemDetails where id=:id");
        query.setParameter("id", id);
        query.executeUpdate();
    }

    public void acceptRentRequest(int id) {
        Query query;
        query = JPA.em().createQuery("UPDATE ItemDetails " +
                "SET acceptRentRequest='yes' where id=:id");
        query.setParameter("id", id);
        query.executeUpdate();
    }

    public void deleteRentRequest(int id) {
        Query query;
        query = JPA.em().createQuery("UPDATE ItemDetails " +
                "SET acceptRentRequest='no', renterId=0 where id=:id");
        query.setParameter("id", id);
        query.executeUpdate();
    }

    public List<ItemDetails> getRentRequestsForUser(int ownerId) {
        TypedQuery query;
        query = JPA.em().createQuery("SELECT i " +
                "from ItemDetails i where i.ownerId=:ownerId " +
                "and i.acceptRentRequest='pending'", ItemDetails.class);
        query.setParameter("ownerId", ownerId);
        return query.getResultList();
    }

    public void saveRentRequest(int itemId, int userId) {
        Query query;
        query = JPA.em().createQuery("UPDATE ItemDetails " +
                "SET acceptRentRequest='pending', renterId=:userId where id=:itemId");
        query.setParameter("itemId", itemId);
        query.setParameter("userId", userId);
        query.executeUpdate();
    }



}
